<!DOCTYPE html >
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <meta name="description" content="">
  <meta name="viewport" content="width=1170">
  <meta http-equiv="content-language" content="ru"> <?php // TODO - в будущем генетить автоматом ?>
  <meta property="og:title" content="Выборы Главы Республики Коми 2014"/>
  <meta property="og:type" content="website"/>
  <meta property="og:url" content="http://xn--2014-43dang2cthqz.xn--p1ai"/>
  <meta property="og:image" content="http://xn--2014-43dang2cthqz.xn--p1ai/1/Gizer.png"/>
  <?php
    //Регистрируем файлы скриптов в <head>
    if (YII_DEBUG) {
      Yii::app()->assetManager->publish(YII_PATH . '/web/js/source', false, -1, true);
    }
    Yii::app()->clientScript->registerCoreScript('jquery');
    $this->registerJsFile('modernizr-2.6.1-respond-1.1.0.min.js', 'ygin.assets.js');

    Yii::app()->clientScript->registerCssFile('/themes/business/dist/css/bootstrap.min.css');
    Yii::app()->clientScript->registerCssFile('/themes/business/css/jquery-ui.min.css');
    Yii::app()->clientScript->registerScriptFile('/themes/business/dist/js/bootstrap.min.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScriptFile('/themes/business/js/jquery-ui.min.js', CClientScript::POS_HEAD);

    Yii::app()->clientScript->registerScriptFile('/themes/business/js/js.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScript('menu.init', "$('.dropdown-toggle').dropdown();", CClientScript::POS_READY);

    $favicon = Yii::getPathOfAlias('webroot.themes.business');
    Yii::app()->clientScript->registerLinkTag('icon', "image/x-icon", Yii::app()->getAssetManager()->publish($favicon) . '/favicon.ico');

    $ass = Yii::getPathOfAlias('webroot.themes.business.dist.fonts') . DIRECTORY_SEPARATOR;
    Yii::app()->clientScript->addDependResource('bootstrap.min.css', array(
      $ass . 'glyphicons-halflings-regular.ttf' => '../fonts/',
      $ass . 'glyphicons-halflings-regular.svg' => '../fonts/',
      $ass . 'glyphicons-halflings-regular.eot' => '../fonts/',
      $ass . 'glyphicons-halflings-regular.woff' => '../fonts/',
    ));

    Yii::app()->clientScript->registerCssFile('/themes/business/css/content.css');
    Yii::app()->clientScript->registerCssFile('/themes/business/css/page.css');
  ?>
  <?php if (Yii::app()->request->url == "/") { ?>
    <?php $this->setPageTitle('Выборы Главы Республики Коми 2014 | ГлаваКоми2014.рф'); ?>
  <?php } ?>
  <title><?php echo CHtml::encode($this->getPageTitle()); ?></title>
</head>
<body>
<div id="wrap" class="container">
  <div id="head" class="row">
    <?php if (Yii::app()->request->url == "/") { ?>
      <div class="text-center slogan">
        Выборы Главы Коми <br> Сделай выбор сегодня!
      </div>
    <?php } else { ?>
      <div class="text-center slogan">
        <a href="/">Выборы Главы Коми <br> Сделай выбор сегодня!</a>
      </div>
    <?php } ?>
    <div class="text-center subslogan">
      14 сентября 2014 года
    </div>
  </div>
  <!-- <div class="b-menu-top navbar">
      <div class="nav-collapse">
<?php
    /*
    if (Yii::app()->hasModule('search')) {
      $this->widget('SearchWidget');
    }
    $this->widget('MenuWidget', array(
      'rootItem' => Yii::app()->menu->all,
      'htmlOptions' => array('class' => 'nav nav-pills'), // корневой ul
      'submenuHtmlOptions' => array('class' => 'dropdown-menu'), // все ul кроме корневого
      'activeCssClass' => 'active', // активный li
      'activateParents' => 'true', // добавлять активность не только для конечного раздела, но и для всех родителей
      //'labelTemplate' => '{label}', // шаблон для подписи
      'labelDropDownTemplate' => '{label} <b class="caret"></b>', // шаблон для подписи разделов, которых есть потомки
      //'linkOptions' => array(), // атрибуты для ссылок
      'linkDropDownOptions' => array('data-target' => '#', 'class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'), // атрибуты для ссылок для разделов, у которых есть потомки
      //'itemOptions' => array(), // атрибуты для li
      'itemDropDownOptions' => array('class' => 'dropdown'),  // атрибуты для li разделов, у которых есть потомки
      'maxChildLevel' => 1,
      'encodeLabel' => false,
    ));
    */
  ?>
      </div>
    </div> -->

  <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_TOP)); ?>

  <?php // + Главный блок ?>
  <div id="main">

    <div id="container" class="row">
      <?php

        $column1 = 0;
        $column2 = 12;
        $column3 = 0;

        if (Yii::app()->menu->current != null) {
          $column1 = 3;
          $column2 = 6;
          $column3 = 3;

          if (Yii::app()->menu->current->getCountModule(SiteModule::PLACE_LEFT) == 0) {
            $column1 = 0;
            $column3 = 4;
          }
          if (Yii::app()->menu->current->getCountModule(SiteModule::PLACE_RIGHT) == 0) {
            $column3 = 0;
            $column1 = $column1 * 4 / 3;
          }
          $column2 = 8 - $column1 - $column3;
        }

      ?>
      <?php if ($column1 > 0): // левая колонка ?>
        <div id="sidebarLeft" class="span<?php echo $column1; ?>">
          <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_LEFT)); ?>
        </div>
      <?php endif ?>

      <?php if (Yii::app()->request->url == "/"){ ?>
      <div id="content" class="col-md-12">
        <?php } else { ?>
        <div id="content" class="col-md-<?php echo $column2; ?> col-md-offset-2">
          <?php } ?>


          <?php if (Yii::app()->request->url != "/") { ?>
            <div class="page-header">
              <h1><?php echo $this->caption; ?></h1>
            </div>
          <?php } ?>
          <?php if (isset($this->breadcrumbs)): // Цепочка навигации ?>
            <?php $this->widget('BreadcrumbsWidget', array(
              'homeLink' => array('Главная' => Yii::app()->homeUrl),
              'links' => $this->breadcrumbs,
            )); ?>
          <?php endif ?>

          <div class="cContent">
            <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_CONTENT)); ?>
            <?php echo $content; ?>
          </div>
          <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_BOTTOM)); ?>

          <?php $this->widget('application.widgets.forecast.ForecastWidget'); ?>


        </div>

        <?php if ($column3 > 0): // левая колонка ?>
          <div id="sidebarRight" class="span<?php echo $column3; ?>">
            <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_RIGHT)); ?>
          </div>
        <?php endif ?>

      </div>
      <?php //Тут возможно какие-нить модули снизу ?>

      <div class="clr"></div>
    </div>
    <?php // - Главный блок ?>

  </div>
  <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_FOOTER)); ?>
  <script type="text/javascript">
    WebFontConfig = {
      google: { families: [ 'Russo+One::cyrillic', 'Noto+Sans::cyrillic', 'Noto+Serif::cyrillic' ] }
    };
    (function () {
      var wf = document.createElement('script');
      wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
        '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
      wf.type = 'text/javascript';
      wf.async = 'true';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(wf, s);
    })(); </script>
</body>
</html>