<div class="b-suggestion js_suggestions_holder">
  <a href="#" class="b-suggestion_link"> Прогноз итогов выборов Главы РК </a>
    <ul id="b-suggestions-sliders" class="list list-unstyled">
      <?php foreach($rate as $item):?>
        <li class="b-suggestions-sliders_container">
          <?php echo $item['name']; ?>
         	<div class="b-suggestions-sliders_progressbar progress">
  				<div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $item['rate'];?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $item['rate'];?>%;">
    				<?php echo $item['rate'];?>%
  				</div>
			</div>
        </li>
      <?php endforeach;?>
    </ul>
</div>