<ul class="answer-list clearfix" id="grid">
        <?php foreach( $voting->answer as $key => $ans ): ?>
                <?php echo CHtml::form (null,null,array('class' => 'b-candidate-form vote-form'.$key)); ?>
                <?php echo CHtml::hiddenField ('id_voting',$voting->id_voting,array('id' => 'id_voting_field')); ?>
                <?php echo CHtml::hiddenField ('vote_widget',1); ?>
                <li class="answer">
                        <?php if ($voting->is_checkbox): ?>
                          <label for="<?php echo "ans_ext_{$ans->id_voting_answer}" ?>" class="b-candidate-form_label checkbox">
                                <?php echo CHtml::activehiddenField ($ans,'name[]',array(
                                        'value' => $ans->id_voting_answer,
                                        //                                      'id' => "ans_ext_{$ans->id_voting_answer}",
                                        //                                      'uncheckValue' => null
                                )); ?>
                                <?php else: ?>
                                <label for="<?php echo "ans_ext_{$ans->id_voting_answer}" ?>" class="b-candidate-form_label radio">
                                        <?php echo CHtml::activehiddenField ($ans,'name',array(
                                                'value' => $ans->id_voting_answer,
                                                //                                              'id' => "ans_ext_{$ans->id_voting_answer}",
                                                //                                              'uncheckValue' => null
                                        )); ?>
                                        <?php endif; ?>
                                        <a href="<?php echo $ans->link; ?>">
                                                <?php echo $ans->name; ?>
                                        </a>
                                </label>
                                <?php if($ans->image): ?>
                                          <a href="<?php echo $ans->link; ?>">
                                                  <img src="<?php echo $ans->image->getPreview (210,245)->getUrlPath (); ?>">
                                          </a>

                                <?php endif; ?>
                                <div class="b-candidate-form_btnVote btnVote">
          <?php if (false): // выборы прошли ?>
                                        <?php
                                                echo CHtml::ajaxSubmitButton ('Голосовать',Yii::app ()->createUrl (VoteModule::ROUTE_VOTE_ACTION),array(
                                                        'type' => 'POST',
//                                                      'replace' => '#vote_widget_' . $voting->id_voting.' .answer-list',
                                                        'dataType' => 'json',
                                                        'success' => 'js: function(data) {
data[0] = 68;
data[1] = 12;
data[2] = 10;
data[3] = 7;
data[4] = 3;
                                                                for (var i=0; i<5; i++) {
                                                                        var form = $(".vote-form"+i);
                                                                        var btn = form.find(".btnVote");
                                                                        btn.html("<p class=\'b-candidate-form_VotesCnt bg-primary\'>Голосов: " + "<span class=\'b-candidate-form_VotesCnt_digits\'>" + data[i] + "%</span></p>");
                                                                }
                                                        }',
                                                ),array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-primary btn-lg',
                                                ));
                                        ?>
          <?php else: ?>
<?php $voteRes = array(69, 11, 9, 8, 3) ?>
            <p class="b-candidate-form_VotesCnt bg-primary ">Голосов: <span class='b-candidate-form_VotesCnt_digits'><?php echo $voteRes[$key]; ?>%</span></p>
<?php /*
            <p class="b-candidate-form_VotesCnt bg-primary ">Голосов: <span class='b-candidate-form_VotesCnt_digits'><?php echo round($ans->count / $voteCount * 100); ?>%</span></p>
*/ ?>
          <?php endif; ?>
                                </div>
                                <div class="b-candidate-form_btnInfo btnInfo">
                                        <a href="<?php echo $ans->link; ?>" class="btn btn-success btn-xs">Информация о кандидате</a>
                                </div>
                </li>
    <?php echo CHtml::endForm (); ?><?php endforeach; ?>
</ul>
