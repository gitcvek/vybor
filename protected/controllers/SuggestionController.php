<?php

  /**
   * Created by PhpStorm.
   * User: Cranky4
   * Date: 03.09.14
   * Time: 12:09
   */
  class SuggestionController extends Controller {

    public function actionAdd() {
      if (!Yii::app()->request->isAjaxRequest) {
        throw new CHttpException(404);
        Yii::app()->end();
      }

      $status = $this->isAvailable();
      if ($status['status'] == 1) {
        $data = Yii::app()->request->getPost('suggestions');

        //проверяем сумму рейтинга, на случай мошенничества
        $total = 0;
        foreach ($data as $rating) {
          $total += $rating;
        }
	      // разрешаем прогноз менее 100%
//        if ($total != 100) {
//          echo CJSON::encode(array(
//            'status' => 0,
//            'text' => 'Сумма баллов должна быть равна 100.'
//          ));
//          Yii::app()->end();
//        }

        //сохраняем прогноз
        $rate = array();
        foreach ($data as $candidate => $rating) {
          $model = new Suggestion();
          $model->ip = HU::getUserIp();
          $model->date = time();
          $model->server = print_r($_SERVER, true);
          $model->id_candidate = $candidate;
          $model->rating = $rating;

          $model->save(false);

          //получаем "вес" кандидата
          $row = Yii::app()->db->createCommand()
            ->select("id_candidate, SUM(rating) as s")
            ->from('pr_suggestion')
            ->where('id_candidate = :IDC', array(
              ':IDC' => $candidate
            ))->group('id_candidate')->queryRow();

          $rate[$row['id_candidate']]['rate'] = $row['s'];
          $rate[$row['id_candidate']]['name'] = VotingAnswer::model()->findByAttributes(array(
            'id_voting' => 1,
            'id_voting_answer' => $candidate
          ))->name;
        }

        //общее количество баллов
        $row = Yii::app()->db->createCommand()
          ->select("SUM(rating) as s")
          ->from('pr_suggestion')
          ->queryRow();
        $total = $row['s'];

        //заменяем "вес" на рейтинг
        $newRate = array();
        foreach ($rate as $candidate_id => $candidate_data) {
          $newRate[$candidate_id] = $candidate_data;
          $newRate[$candidate_id]['rate'] = round($candidate_data['rate']*100/$total);
        }

        //ставим куку дя блока
        Yii::app()->request->cookies['suggested'] = new CHttpCookie('suggested', 1);

        echo CJSON::encode(array(
          'status' => 1,
          'text' => $this->renderPartial('suggestionRatings', array(
                'rate' => $newRate,
              ), true)
        ));
        Yii::app()->end();
      } else {
        echo CJSON::encode($status);
        Yii::app()->end();
      }
    }

    private function isAvailable() {
      if (!$ip = HU::getUserIp()) {
        return array(
          'status' => 0,
          'text' => 'Ошибка получения вашего IP. Повторите попытку позднее.'
        );
      }
      if (Suggestion::model()->countByAttributes(array('ip' => $ip))) {
        return array(
          'status' => 0,
          'text' => 'С вашего ip уже получен прогноз. Спасибо!'
        );
      }
      return array(
        'status' => 1,
        'text' => ''
      );
    }
  }