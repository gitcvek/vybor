<?php

/**
 * Модель для таблицы "pr_vote".
 *
 * The followings are the available columns in table 'pr_vote':
 * @property integer $id_vote
 * @property integer $id_voting_answer
 * @property string $ip
 * @property string $date
 *
 * The followings are the available model relations:
 * @property VotingAnswer $votingAnswer
 */
class Vote extends DaActiveRecord {

  const ID_OBJECT = 'project-golosa';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Vote the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'pr_vote';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('id_voting_answer, ip, date', 'required'),
      array('id_voting_answer', 'numerical', 'integerOnly'=>true),
      array('ip', 'length', 'max'=>255),
      array('date', 'length', 'max'=>10),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      'votingAnswer' => array(self::BELONGS_TO, 'VotingAnswer', 'id_voting_answer'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_vote' => 'ID',
      'id_voting_answer' => 'Ответ',
      'ip' => 'IP',
      'date' => 'Дата',
    );
  }

}