<?php

/**
 * Модель для таблицы "pr_suggestion".
 *
 * The followings are the available columns in table 'pr_suggestion':
 * @property integer $id_suggestion
 * @property string $ip
 * @property integer $id_candidate
 * @property string $date
 * @property string $server
 * @property integer $rating
 *
 * The followings are the available model relations:
 * @property VotingAnswer $candidate
 */
class Suggestion extends DaActiveRecord {

  const ID_OBJECT = 'project-predskazanie';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Suggestion the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'pr_suggestion';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('ip, id_candidate, rating', 'required'),
      array('id_candidate, rating', 'numerical', 'integerOnly'=>true),
      array('ip', 'length', 'max'=>255),
      array('date', 'length', 'max'=>10),
      array('server', 'safe'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      'candidate' => array(self::BELONGS_TO, 'VotingAnswer', 'id_candidate'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_suggestion' => 'ID',
      'ip' => 'IP',
      'id_candidate' => 'Кандидат',
      'date' => 'Дата',
      'server' => 'Сервер',
      'rating' => 'Рейтинг',
    );
  }

}