<?php
  Yii::app()->clientScript->registerScriptFile(
      Yii::app()->assetManager->publish(
          Yii::getPathOfAlias('ygin.widgets.alert.assets').'/daAlert-min.js'
      ),
      CClientScript::POS_END
  );
  Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.widgets.forecast.assets') . '/forecast.js'), CClientScript::POS_END);

?>
<div class="b-suggestion js_suggestions_holder">
  <a href="#" class="b-suggestion_link">Сделайте прогноз итогов выборов Главы РК</a>

  <!-- слайдеры -->

  <form class="l-suggestions">
  <ul id="b-suggestions-sliders" class="list list-unstyled">
    <?php foreach($candidates as $candidate):?>
      <li class="b-suggestions-sliders_container">
        <?php echo $candidate->name; ?> <span class="b-suggestions-sliders_value js_rating">0</span>
        <?php echo CHtml::hiddenField('suggestions['.$candidate->id_voting_answer.']',0,array('class' => 'js_hinput'))?>

        <div id="adult" class="b-suggestions-sliders_slider"></div>
      </li>
    <?php endforeach;?>
  </ul>
    <?php echo CHtml::ajaxSubmitButton('Отправить',Yii::app()->createUrl('suggestion/add'),array(
      'beforeSend' =>'js: function() {
        $("#ajaxLoader").show();
      }',
      'dataType'=> 'json',
      'success' => 'js: function(msg) {
        $("#ajaxLoader").hide();
        if(msg.status*1 == 1) {
          $(".js_suggestions_holder").replaceWith(msg.text);
        } else {
          daAlert("Извините", msg.text,"OK","");
        }
      }'),array(
        'class' => 'btn btn-primary btn-lg js_suggestion_send',
        //'disabled' => 'disabled'
    ));?>
  </form>

</div>
