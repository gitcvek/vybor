var sliders = $("#b-suggestions-sliders .b-suggestions-sliders_slider"),
    slider_max = 100,
    slider_min = 0,
    submitBtn = $('.js_suggestion_send');

sliders.each(function () {
    var value = parseInt($(this).text(), 10),
        availableTotal = slider_max;

    $(this).empty().slider({
        value: 0,
        min: slider_min,
        max: slider_max,
        range: 'min',
        step: 1,
        start: function (event, ui) {
            var oldValue = ui.value;
        },
        stop: function (event, ui) {
            // получение текущего значения
            var total = 0;
            sliders.each(function () {
                total += $(this).slider("option", "value");
            });
            if (total <= 100) {
                $(this).siblings('.js_rating').text(ui.value);
                $(this).siblings('.js_hinput').val(ui.value);
                //submitBtn.attr('disabled',true);
            } else {
                //выставляем ползунок на максимум от того что можно еще выставить
                otherTotal = 0;
                sliders.not(this).each(function () {
                    otherTotal += $(this).slider("option", "value");
                });
                newValue = 100 - otherTotal;
                $(this).slider("option", "value", newValue);
                $(this).siblings('.js_rating').text(newValue);
                $(this).siblings('.js_hinput').val(newValue);
                submitBtn.attr('disabled',false);
            }
            if(total == 100) {
                submitBtn.attr('disabled',false);
            }

        },
        slide: function (event, ui) {
            $(this).siblings('.js_rating').text(ui.value);
        }
    });
});