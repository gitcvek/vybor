<?php
/**
 * Created by PhpStorm.
 * User: Cranky4
 * Date: 03.09.14
 * Time: 10:03
 */

class ForecastWidget extends DaWidget {
  //ид голосовалки с кандидатами
  private $id_vote = 1;

  public function run() {
    //if(HArray::val(Yii::app()->request->cookies,'suggested')) {
    if(true) { // выборы прошли
      //получаем "вес" кандидатов
      $rows = Yii::app()->db->createCommand()
        ->select("id_candidate, SUM(rating) as s")
        ->from('pr_suggestion')
        ->group('id_candidate')->queryAll();

      $rate = array();
      foreach ($rows as $row) {
        $rate[$row['id_candidate']]['rate'] = $row['s'];
        $rate[$row['id_candidate']]['name'] = VotingAnswer::model()->findByAttributes(array(
          'id_voting' => 1,
          'id_voting_answer' => $row['id_candidate']
        ))->name;
      }


      //общее количество баллов
      $row = Yii::app()->db->createCommand()
        ->select("SUM(rating) as s")
        ->from('pr_suggestion')
        ->queryRow();
      $total = $row['s'];

      //заменяем "вес" на рейтинг
      $newRate = array();
      foreach ($rate as $candidate_id => $candidate_data) {
        $newRate[$candidate_id] = $candidate_data;
          $num = $candidate_data['rate']*100/$total;
          $newRate[$candidate_id]['rate'] = round($num);
        }

      $this->render('webroot.themes.business.views.suggestion.suggestionRatings', array(
        'rate' => $newRate,
      ));
    } else {
      $candidates = VotingAnswer::model()->findAllByAttributes(array('id_voting' => $this->id_vote));
      $this->render('forecast', array(
        'candidates' => $candidates,
      ));
    }

  }

}